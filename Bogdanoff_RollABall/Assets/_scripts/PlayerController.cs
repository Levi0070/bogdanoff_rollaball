﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement; //lets you use scene managing code in script


[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    bool onGround = true; //if the player is on the ground
    public float jumpPower; //jump height, set in editor

    public float speedBoostTimer = 2f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }
    void Update()
    {
        //shoots a line below the player that extends .52f downwards, 
        //if it hits something return true (we on ground)
        //if not return false (we in air)
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);

        if (Input.GetButtonDown("Jump") && onGround) //checks if player presses jump and is on the ground
        {
            Jump();
        }

        if (transform.position.y < -100f)
        {
            RestartLevel();
        }

        if (speedBoostTimer > 0f) //when there is time left in the speed boost
        {
            speedBoostTimer -= Time.deltaTime; //subtract time from timer
            if (speedBoostTimer < 0f) //if time has run out
            {
                speed = speed / 4f; //set speed back to normal
            }
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }

        if (other.gameObject.CompareTag("SpeedPickUp"))
        {   //if trigger collider we collide with is tagged "SpeedPickUp"
            other.gameObject.SetActive(false); //deactivate the pickup
            BoostSpeed(); //BOOST
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 25)
        {
            winText.text = "You Win!";

            Invoke("RestartLevel", 2f); //call level restart after 2 sec
        }
    }

    void Jump()
    {
        rb.AddForce(Vector3.up * jumpPower); //add force based on jump power upwards to jump
    }
    
    void RestartLevel()
    {   //reload current level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    void BoostSpeed()
    {
        speed = speed * 4f;
    }
}